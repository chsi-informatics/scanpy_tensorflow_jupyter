# Running
## Basic
```
singularity run scanpy_tensorflow_jupyter.sif
```

## Running a DCC

We will use the example of the Duke Computer Cluster, but these instructions should be easily adaptable to other clusters

1. From your computer run this to connect to DCC:
```
ssh NetID@dcc-login-03.oit.duke.edu
```
2. Once you are connected run this to start a tmux session:
```
tmux new -s jupyter
```
3. Once you have started a tmux session you can start up Jupyter with this command:
```
srun singularity run /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```

Running this command will print a bunch of stuff. You can ignore everything except the last two lines, which will say something like:
```
http://dcc-chsi-01:8889/?token=08172007896ad29bb5fbd92f6f3f516a8b2f7303ed7f1df3
or http://127.0.0.1:8889/?token=08172007896ad29bb5fbd92f6f3f516a8b2f7303ed7f1df3
```

You need this information for the next few steps. For the next step you need the “dcc-chsi-01:8889” part.
“dcc-chsi-01” is the compute node that Jupyter is running on and “8889” is the port it is listening on. You may get a different value every time you start the container.

4. You want to run the following command in another terminal on your computer to set up port forwarding.
```
ssh -L PORT:NODE.rc.duke.edu:PORT NetID@dcc-login-03.oit.duke.edu
```
In this command you want to replace “PORT” with the value you got for port from the srun command and replace “NODE” with the compute node that was printed by the srun command. So for the example above, the ssh port forwarding command would be:

```
ssh -L 8889:dcc-chsi-01.rc.duke.edu:8889 NetID@dcc-login-03.oit.duke.edu
```

5. Now you can put the last line that the srun command printed in your web browser and it should open your Jupyter instance running on DCC.

### More Memory or More CPUs
If you need more memory or more cpus you can use the `--mem` and/or `--cpus-per-task` arguments to in the “srun”, for example to request 4 CPUs and 10GB of RAM:

```
srun --cpus-per-task=4 --mem=10G singularity run /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```

### Specifying a Partition
If you have high priority access to a partition you can request that partition be used with the `-A` and `-p` arguments to `srun`:

```
srun -A chsi -p chsi srun singularity run /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```

### Use a GPU
A few modifications are necessary to be able to use a GPU within the container:

1. Use partition `chsi-gpu`
2. Request a GPU with `--gres=gpu:1`
3. Run singularity with the `--nv` flag

Here is an example:

```
 srun -A chsi -p chsi-gpu --gres=gpu:1 --mem=0 --cpus-per-task=6 \
    singularity run --nv /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```

> Run the following as a general test that you can jobs on a GPU
```
srun -A chsi -p chsi-gpu --gres=gpu:1 --mem=0 --cpus-per-task=6 nvidia-smi
```
It should output something like this:
```
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 495.29.05    Driver Version: 495.29.05    CUDA Version: 11.5     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  NVIDIA GeForce ...  Off  | 00000000:13:00.0 Off |                  N/A |
| 33%   33C    P0     1W / 250W |      0MiB / 11019MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

### Bind Mount Host Directories
You might want to access files that are outside of your home directory. Within a singularity container your access to the host computer is limited: by default, from inside the container you can only access your home directory. If you want to access directories that are outside your home directory, you have to tell *Singularity* when you start the container with the `--bind` command line argument. For example:

The following is an example, host directory paths need to be customized

```
BASE_DIR="/work/${USER}/pomms"
DATA_SRC="$BASE_DIR/rawdata"
WORKSPACE_SRC="$BASE_DIR/workspace"
mkdir -p $DATA_SRC $WORKSPACE_SRC

singularity run --bind ${DATA_SRC}:/data --bind ${WORKSPACE_SRC}:/workspace /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```

Or 

```
srun singularity run --bind /work/${USER}:/work/${USER} /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```


### Combining Complex Options

You can combine several of these command line flags:
```
srun -A chsi -p chsi --cpus-per-task=4 --mem=10G singularity run /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```




### Run From Registry
#### Generate Container Registry Token
This only needs to be done once, before you pull containers from the Container Registry oras://gitlab-registry.oit.duke.edu/ for the first time

You need to run `singularity remote login` to generate a login token. 

To do this, run the following command where [username] is your username for https://gitlab.oit.duke.edu/ (probably your NetID):
```
singularity remote login --username [username] oras://gitlab-registry.oit.duke.edu
```
When asked for your `Password / Token:`, supply your password for https://gitlab.oit.duke.edu/ (this may be your NetID password if you have not set up a separate password)

#### Run 
To do an implicit pull of the image from the registry at runtime, `oras://gitlab-registry.oit.duke.edu/chsi-informatics/scanpy_tensorflow_jupyter:latest`
 can be substituted for the SIF file path (e.g. `scanpy_tensorflow_jupyter.sif`) in any of the `singularity run` commands given in these instructions. For example:

```
srun -A chsi -p chsi singularity run oras://gitlab-registry.oit.duke.edu/chsi-informatics/scanpy_tensorflow_jupyter:latest
```
Note that before the first time you do this, you must have [generated a token](#generate-container-registry-token).

Also be sure to read the [recommendation for setting SINGULARITY_CACHEDIR](#singularity_cachedir).

### Notes
1. The Jupyter session keeps running until you explicitly shut it down.  If the port forwarding SSH connection drops you will need to restart SSH with the same command, but you don’t need to restart Jupyter.

2. There are two ways to explicitly shut down Jupyter:
    1. Within Jupyter, click on the *Jupyter* logo in the top left to go to the main Jupyter page, then click "Quit" in the top right
    2. Do control-C twice in the terminal where you started Jupyter. If this connection dropped, you can reconnect to it with:
    ```
    ssh NetID@dcc-login-03.oit.duke.edu
    tmux a -t jupyter
    ```
    After shutting down the Jupyter session you can type `exit` at the terminal to close the tmux session.


# Download Image
## cURL
The image can be downloaded with this command

```
curl -O https://research-singularity-registry.oit.duke.edu/chsi-informatics/scanpy_tensorflow_jupyter.sif
```
### Singularity Pull 
The image can also be downloaded with `singularity pull` using the following command.
Note that before the first time you do this, you must have [generated a token](#generate-container-registry-token).

```
srun -A chsi -p chsi singularity pull oras://gitlab-registry.oit.duke.edu/chsi-informatics/scanpy_tensorflow_jupyter:latest
```

#### SINGULARITY_CACHEDIR
It is strongly recommended to set the `SINGULARITY_CACHEDIR` environment variables in your .bashrc or when running `srun`. This environment variable specifies where the Docker image (and the Singularity image built from it) are saved. If this variable is not specified, singularity will cache images in `$HOME/.singularity/cache`, which can fill up quickly. This is discussed in the [Singularity Documentation](https://sylabs.io/guides/3.7/user-guide/build_env.html#cache-folders)

```
export SINGULARITY_CACHEDIR="/work/${USER}/singularity_cache"; srun singularity run /hpc/group/chsi/container_images/scanpy_tensorflow_jupyter.sif
```




# Build
Pushing to gitlab automatically builds the image.  Details are at <https://gitlab.oit.duke.edu/OIT-DCC/singularity-example>


# Notes on Gitlab CI/CD
https://gitlab.oit.duke.edu/jmnewton/lidsky-primers

https://gitlab.oit.duke.edu/devil-ops/singularity-jupyter

You might be interested in this project that Nate Childers created: https://gitlab.oit.duke.edu/devil-ops/singularity-jupyter.
 
If you look at the .gitlab-ci.yml you can see he uses rule that say only starts the build if the definition file is changed. Then the deploy part actually uses the CI_COMMIT_TAG instead of latest. This does mean you have to do a git tag before you push a change to a definition. It brings up the scenario where you could use a single project with multiple definition files and the build for each would only get kicked off if you changed that file. You could then have a library of images in a registry for one project instead of separate projects. But then that may be putting all your eggs in one basket.
